$(document).ready(function () {
    $(".accodition").click(function () {
        $(this).next().slideToggle('fast');
        $(this).toggleClass('rotate');
    });
    $(".open-sidemenu").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $('.block-overlay').toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').addClass('slow-layer');
    });
    $(".block-overlay").click(function () {
        $('#sidenav').toggleClass('menu-mobile');
        $(this).toggleClass('over');
        $('body').toggleClass('no-scroll');
        $('.block-header-sale').removeClass('slow-layer');
    });
    $( window ).scroll(function() {
        if($(window).scrollTop() === $(".menu").offset().top){
            $(".menu").addClass('isTop');
        }else{
            $(".menu").removeClass('isTop');
        }
    });
});